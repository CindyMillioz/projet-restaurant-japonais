# Projet - Restaurant Japonais

## Tech
- HTML
- CSS
- Bootstrap

## Architecture
- index.html
- readme.md
    - images:
        - background.png
        - chopsticks.png
        - footer-bg.png
        - footer-shadow.png
        - header-bg.png
        - logo-bg.png
        - modele-restaurant-japonais.png
        - pepper.jpg
        - sushi1-241x167.jpg
        - sushi.jpg
        - sushi2-241x167.jpg
        - sushi3-241x167.jpg
        - sushi3.jpg
        - sushi4-241x167.jpg
        - sushi4.jpg
    - CSS :
     - style.css
     - responsive.css

## Consignes :
-  Réaliser une page web selon une maquette existante
- Faire une bonne architecture de projet 
- Utiliser HTML/CSS
- Utiliser Bootstrap
- Faire un site responsive